// Copyright (c) 2020 privb0x23

// 0.1.1+2020-09-23

// -----------------------------------------------------------------------------

#![deny(unsafe_code)]

use tracing::{
	// debug,
	error,
	// event,
	info,
	// instrument,
	// span,
	trace,
	// warn,
	// Level,
};

// use tracing_futures::Instrument as _;

use bastion::{
	context::BastionContext, dispatcher::BroadcastTarget, envelope::SignedMessage, msg, Bastion,
};
// use bastion::resizer::{ OptimalSizeExploringResizer, UpperBound, UpscaleStrategy };

use ::std::sync::Arc;
use ::std::time::Duration;

// -----------------------------------------------------------------------------

pub type Result<T> = anyhow::Result<T>;

pub type Craft = bastion::supervisor::SupervisorRef;

pub type Children = bastion::children::Children;
pub type Supervisor = bastion::supervisor::Supervisor;
pub type Dispatcher = bastion::dispatcher::Dispatcher;
pub type DispatcherType = bastion::dispatcher::DispatcherType;

const VERSION: &str = "0.1.1+2020-09-23";
const BIND_IPV4_HTTP: &str = "127.0.0.1";
const BIND_PORT_HTTP: u16 = 8080;

// -----------------------------------------------------------------------------

#[derive(thiserror::Error, Debug)]
pub enum CustomError {
	#[error("unknown error")]
	Unknown,
}

// -----------------------------------------------------------------------------

pub fn main() -> Result<()> {
	tracing::subscriber::set_global_default(
		tracing_subscriber::FmtSubscriber::builder()
			.with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
			.finish(),
	)?;

	info!("bastion-tide v{}", VERSION);

	let code = {
		if let Err(e) = run() {
			error!("error: {}", e);
			1
		} else {
			0
		}
	};

	::std::process::exit(code);
}

// -----------------------------------------------------------------------------

fn run() -> Result<()> {
	info!("Starting…");

	Bastion::init();

	let _c2: Craft = deploy_craft_c2()?;

	let _some_thing: Craft = deploy_craft_some_thing()?;

	let _web_app: Craft = deploy_craft_web_app()?;

	Bastion::start();
	Bastion::block_until_stopped();

	Ok(())
}

fn deploy_craft_web_app() -> Result<Craft> {
	match Bastion::supervisor(|supervisor| {
		supervisor.children(|children| {
			children.with_redundancy(1).with_heartbeat_tick(Duration::from_secs(20)).with_exec(
				|ctx: BastionContext| {
					trace!("run web app");

					let arc_ctx = Arc::new(ctx);

					// state

					let webapp = web_app::WebApp::new(Arc::clone(&arc_ctx));

					let mut app: web_app::WebServer = tide::with_state(webapp);

					// routes
					do_routes(&mut app);

					async move {
						// server

						if let Err(e) =
							app.listen(format!("{}:{}", BIND_IPV4_HTTP, BIND_PORT_HTTP)).await
						{
							error!("error: {}", e);

							Err(())
						} else {
							Ok(())
						}
					}
				},
			)
		})
	}) {
		Ok(c) => Ok(c),
		Err(_) => Err(anyhow::anyhow!("deploy craft failed")),
	}
}

fn do_routes(app: &mut web_app::WebServer) {
	app.at("/a/1/health_check").get(web_app::handle_health_check);
}

fn deploy_craft_some_thing() -> Result<Craft> {
	match Bastion::supervisor(|supervisor| {
		supervisor.children(|children| {
			children
			.with_redundancy( 3 )
			.with_heartbeat_tick(
				Duration::from_secs( 20 )
			)
		//	.with_resizer(
		//		OptimalSizeExploringResizer::default()
		//		.with_lower_bound( 1 )
		//		.with_upper_bound(
		//			UpperBound::Limit( 10 )
		//		)
		//		.with_upscale_strategy(
		//			UpscaleStrategy::MailboxSizeThreshold( 3 )
		//		)
		//		.with_upscale_rate( 0.1 )
		//		.with_downscale_rate( 0.2 )
		//	)
			.with_dispatcher(
				Dispatcher::with_type(
					DispatcherType::Named(
						do_some_thing::GROUP_TARGET_1.to_string()
					)
				)
			)
			.with_exec( | ctx : BastionContext | {
				async move {

					trace!(
						"run some thing"
					);

					{
						let target = BroadcastTarget::Group(
							"c2".to_string()
						);

						ctx.broadcast_message( target, "register" );
					}

					loop {

						// receive message
						msg! {
							ctx.recv().await?,

							raw_message : Arc< SignedMessage > => {

								trace!(
									"some thing message received"
								);

								let message = Arc::try_unwrap( raw_message );

								if message.is_ok() {

									// receive message
									msg! {
										message.unwrap(),

										ref data : & str => {

											info!(
												"Received {}",
												data
											);

											let res = do_some_thing::run()
												.await;

											info!(
												"result: {}",
												res
											);
										};
										_: _ => ();
									};

								} else {
									error!(
										"error unpacking message"
									);
								}
							};
							unknown:_ => {
								error!(
									"invalid message\n{:?}",
									unknown
								);
							};
						}
					}
				}
			} )
		})
	}) {
		Ok(c) => Ok(c),
		Err(_) => Err(anyhow::anyhow!("deploy craft failed")),
	}
}

fn deploy_craft_c2() -> Result<Craft> {
	match Bastion::supervisor(|supervisor| {
		supervisor.children(|children| {
			children
			.with_redundancy( 2 )
			.with_heartbeat_tick(
				Duration::from_secs( 20 )
			)
		//	.with_resizer(
		//		OptimalSizeExploringResizer::default()
		//		.with_lower_bound( 1 )
		//		.with_upper_bound(
		//			UpperBound::Limit( 10 )
		//		)
		//		.with_upscale_strategy(
		//			UpscaleStrategy::MailboxSizeThreshold( 3 )
		//		)
		//		.with_upscale_rate( 0.1 )
		//		.with_downscale_rate( 0.2 )
		//	)
			.with_dispatcher(
				Dispatcher::with_type(
					DispatcherType::Named(
						"c2".to_string()
					)
				)
			)
			.with_exec( | ctx : BastionContext | {
				async move {

					trace!(
						"run c2"
					);

					loop {

						// receive message
						msg! {
							ctx.recv().await?,

							raw_message : Arc< SignedMessage > => {

								trace!(
									"c2 message received"
								);

								let message = Arc::try_unwrap( raw_message );

								if message.is_ok() {

									// receive message
									msg! {
										message.unwrap(),

										ref data : & str => {

											info!(
												"Received {}",
												data
											);

											match *data {

												"register" => {
													trace!( "register" );
												},

												"health check" => {
													trace!( "some thing message" );

													{
														let target = BroadcastTarget::Group(
															do_some_thing::GROUP_TARGET_1.to_string()
														);

														ctx.broadcast_message( target.clone(), "register" );
													}
												},

												_ => { trace!( "no-op" ); }
											}

										};
										_: _ => ();
									}

								} else {
									error!(
										"error unpacking message"
									);
								}
							};
							unknown:_ => {
								error!(
									"invalid message\n{:?}",
									unknown
								);
							};
						}
					}
				}
			} )
		})
	}) {
		Ok(c) => Ok(c),
		Err(_) => Err(anyhow::anyhow!("deploy craft failed")),
	}
}

// -----------------------------------------------------------------------------

pub mod do_some_thing {

	use ::std::time::Duration;
	use futures_timer::Delay;
	use tracing::trace;

	pub const GROUP_TARGET_1: &str = "SomeThing";

	#[derive(Debug)]
	pub struct SomeThing {
		pub uuid: String,
	}

	impl ::std::fmt::Display for SomeThing {
		fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> ::std::fmt::Result {
			write!(f, "{}", self.uuid)
		}
	}

	pub async fn run() -> SomeThing {
		trace!("some_thing run");

		Delay::new(Duration::from_secs(5)).await;

		SomeThing {
			uuid: String::from("ok"),
		}
	}
}

// -----------------------------------------------------------------------------

pub mod web_app {

	use bastion::context::BastionContext;
	use tracing::trace;
	pub type BroadcastTarget = bastion::dispatcher::BroadcastTarget;
	use ::std::sync::Arc;

	pub type WebServer = tide::Server<WebApp>;
	type WebRequest = tide::Request<WebApp>;

	// state / context for web app
	#[derive(Clone)]
	pub struct WebApp {
		ctx: Arc<BastionContext>,
	}

	pub struct WebAppInstance {
		pub routes: Box<dyn Fn(&mut WebServer)>,
	}

	impl WebAppInstance {
		pub fn new(routes: Box<dyn Fn(&mut WebServer)>) -> ::std::boxed::Box<Self> {
			::std::boxed::Box::new(WebAppInstance {
				routes,
			})
		}
	}

	impl WebApp {
		pub fn new(ctx: Arc<BastionContext>) -> Self {
			trace!("new WebApp");

			Self {
				ctx,
			}
		}
	}

	impl WebApp {
		// runs a task when requested
		pub async fn health_check(&self) -> super::Result<super::do_some_thing::SomeThing> {
			trace!("WebApp health_check");

			let target = BroadcastTarget::Group("c2".to_string());

			self.ctx.broadcast_message(target, "health check");

			Ok(super::do_some_thing::SomeThing {
				uuid: String::from("dummy"),
			})
		}
	}

	// this handles a web request & calls the appropriate webapp function
	pub async fn handle_health_check(
		req: WebRequest,
	) -> ::std::result::Result<String, tide::Error> {
		trace!("web_app handle_health_check");

		let res =
			req.state().health_check().await.map_err(|_| {
				tide::Error::from_str(tide::StatusCode::InternalServerError, "fail")
			})?;

		Ok(format!("{}\n", res))
	}
}

// -----------------------------------------------------------------------------

// eof main.rs
