// Copyright (c) 2020 privb0x23

// 0.1.1+2020-09-23

// -----------------------------------------------------------------------------

#![deny(unsafe_code)]

use tracing::error;

// use tracing_futures::Instrument as _;

// -----------------------------------------------------------------------------

pub fn main() -> anyhow::Result<()> {
	tracing::subscriber::set_global_default(
		tracing_subscriber::FmtSubscriber::builder()
			.with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
			.finish(),
	)?;

	/*
	tracing::subscriber::set_global_default(
		tracing_subscriber::FmtSubscriber::builder()
		.with_max_level(
			tracing::Level::TRACE
		)
		.finish()
	)?;
	*/

	let code = {
		if let Err(e) = blank_lib::run() {
			error!("error: {}", e);
			1
		} else {
			0
		}
	};

	::std::process::exit(code);
}

// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------

// eof main.rs
