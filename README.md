# Rust

## Tips

Some useful commands:

```sh
# for binary
cargo new project_name --bin
# for library
cargo new project_name --lib
cd project_name
cargo check
cargo clippy
cargo fmt
cargo build
cargo build --release
cargo build --release --target=x86_64-unknown-linux-musl
RUST_LOG='trace' cargo run
```

Some useful tools:

```sh
mkdir -v ~/.zfunc
rustup completions zsh > ~/.zfunc/_rustup
# requires libressl/openssl development files
cargo install cargo-outdated
cargo install cargo-audit
cargo outdated
cargo audit
```

Some useful configuration:

`Cargo.toml`

```ini
[profile.release]
lto = true
codegen-units = 1
opt-level = "z"
```

`.cargo/config`

```ini
[build]
target = "x86_64-unknown-linux-musl"

[target.x86_64-unknown-linux-musl]
rustflags = [ "-C", "target-feature=+crt-static "]

[target.x86_64-pc-windows-msvc]
rustflags = [ "-C", "target-feature=+crt-static" ]
```

## Install rust

```sh
curl --url 'https://sh.rustup.rs/' --output 'rustup-init.sh'
chmod -v 755 rustup-init.sh
export PATH="${PATH}:${HOME}/.cargo/bin"
export TMPDIR='/var/tmp'
./rustup-init.sh -v -y --no-modify-path
rustup target add x86_64-unknown-linux-musl
rustup target add x86_64-unknown-linux-gnu
rustup target add x86_64-pc-windows-msvc
rustup component add clippy
rustup component add rustfmt
```

## Install sstrip

```sh
curl --location --url 'https://github.com/BR903/ELFkickers/archive/master.tar.gz' --output '/var/tmp/ELFkickers.tgz'
mkdir -vp '/var/tmp/ELFkickers'
tar -x --strip-components 1 -C '/var/tmp/ELFkickers' -f '/var/tmp/ELFkickers.tgz'
cd '/var/tmp/ELFkickers'
sed -i'' 's/CC = gcc/CC = clang/g' ./elfrw/Makefile ./sstrip/Makefile
make sstrip
strip ./bin/sstrip
sudo install -m755 ./bin/sstrip /usr/local/bin/
```

## Resources

- https://play.rust-lang.org/
- https://doc.rust-lang.org/rust-by-example/
- https://doc.rust-lang.org/book/
- https://doc.rust-lang.org/stable/core/
- https://learning-rust.github.io/
- https://rust-lang-nursery.github.io/rust-cookbook/
- https://lib.rs/
- https://cheats.rs/
- https://rust-lang.github.io/async-book/
- https://stevedonovan.github.io/rust-gentle-intro/
- https://zsiciarz.github.io/24daysofrust/
- https://fasterthanli.me/blog/2020/a-half-hour-to-learn-rust/
- https://anssi-fr.github.io/rust-guide/
- https://www.ssi.gouv.fr/uploads/2020/06/anssi-guide-programming_rules_to_develop_secure_applications_with_rust-v1.0.pdf
- https://ferrous-systems.com/blog/mindsets-and-expectations/
- https://tiemoko.com/blog/blue-team-rust/
- https://hashrust.com/blog/memory-safey-in-rust-part-1/
- https://hashrust.com/blog/memory-safety-in-rust-part-2/
- https://medium.com/@orbitalK/why-the-machine-b9803a77fa29
- https://deepu.tech/memory-management-in-rust/
- https://rufflewind.com/img/rust-move-copy-borrow.png
- https://gradebot.org/doc/ipur/
- https://omarabid.com/rust-intro
- https://www.geekabyte.io/2020/01/learning-rust-day-1.html
- https://rust-cli.github.io/book/
- https://riptutorial.com/rust/topic/362/getting-started-with-rust
- https://medium.com/@polyglot_factotum/rust-regret-less-concurrency-2238b9e53333
- https://www.hellsoft.se/understanding-cpu-and-i-o-bound-for-asynchronous-operations/
- https://bryce.fisher-fleig.org/blog/strategies-for-returning-references-in-rust/
- https://chaoslibrary.blot.im/rust-cloning-a-trait-object
- https://deislabs.io/posts/a-fistful-of-states/
- https://nick.groenen.me/posts/rust-error-handling/
- https://github.com/dtolnay/thiserror
- https://github.com/dtolnay/anyhow
- https://github.com/tokio-rs/tracing
- https://github.com/Amanieu/parking_lot
- https://nrxus.github.io/faux/
- https://github.com/kbknapp/cargo-outdated
- https://github.com/RustSec/cargo-audit
- https://github.com/rust-unofficial/patterns
- https://github.com/brson/stdx
- https://github.com/rust-rosetta/rust-rosetta
- https://bastion.rs/
- https://github.com/bastion-rs/agnostik
- https://github.com/crossbeam-rs/crossbeam
- https://github.com/iqlusioninc/crates/tree/develop/zeroize
- https://github.com/xacrimon/dashmap
- https://github.com/mehcode/config-rs
- https://github.com/djc/quinn
- https://github.com/maidsafe/quic-p2p
- https://github.com/mcginty/snow
- https://github.com/nlnetlabs/domain
- https://github.com/cloudflare/boringtun
- https://github.com/flier/rust-hyperscan
- https://github.com/ChosunOne/merkle_bit
- https://github.com/filecoin-project/merkle_light
- https://github.com/BLAKE3-team/BLAKE3
- https://github.com/flier/rust-fasthash
- https://github.com/rustysec/fuzzyhash-rs
- https://github.com/RustCrypto/hashes
- https://github.com/typed-io/cryptoxide
- https://github.com/oconnor663/duct.rs
- https://github.com/bluejekyll/trust-dns
- https://github.com/fMeow/arangors
- https://github.com/tokio-rs/tokio-socks5
- https://tokio.rs/
- https://hyper.rs/
- https://github.com/seanmonstar/warp
- https://github.com/seanmonstar/reqwest
- https://github.com/actix/actix
- https://github.com/LeonHartley/Coerce-rs
- https://github.com/rsimmonsjr/axiom
- https://github.com/Smithay/calloop
- https://zeromq.org/
- https://github.com/erickt/rust-zmq
- https://github.com/datrs/hypercore
- https://github.com/RustPython/RustPython
- https://github.com/KDE/heaptrack
- https://github.com/ajmwagar/merino
- https://www.snoyman.com/blog/2018/11/rust-crash-course-06-lifetimes-slices
- https://www.snoyman.com/blog/2019/12/rust-crash-course-09-tokio-0-2
- https://google.github.io/flatbuffers/flatbuffers_guide_use_rust.html
- https://noiseprotocol.org/noise.html#interactive-handshake-patterns-fundamental
- https://michael-f-bryan.github.io/rust-ffi-guide/dynamic_loading.html
- https://s3.amazonaws.com/temp.michaelfbryan.com/dynamic-loading/index.html
- https://github.com/luojia65/plugin-system-example
- https://gitlab.com/rust_musl_docker/image
- https://github.com/clux/muslrust
- https://github.com/rust-embedded/cross
- https://github.com/emk/rust-musl-builder
- https://doc.rust-lang.org/edition-guide/rust-2018/platform-and-target-support/musl-support-for-fully-static-binaries.html
- https://os.phil-opp.com/freestanding-rust-binary/
- https://lifthrasiir.github.io/rustlog/why-is-a-rust-executable-large.html
- https://www.reddit.com/r/rust/comments/61pjbp/static_linking_support/
- https://blog.nodraak.fr/2020/07/rust-wasm-2-hello-world/
